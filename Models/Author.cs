﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace library_project.Models
{
    public class Author
    {
        public int Id { get; set; }
        public String autName { get; set; }
        public String autLastName { get; set; }
        public String autCountry { get; set; }
    }
}
