﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace library_project.Models
{
    public class Book
    {
        public int Id { get; set;}
        public String bookName { get; set; }
        public String bookCode { get; set; }
        public Author bookAuthor { get; set; }
        public Category bookCategory { get; set; }
        public String bookPublicationDate { get; set; }
    }
}
