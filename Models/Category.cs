﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace library_project.Models
{
    public class Category
    {
        public int Id { get; set; }
        public String catName { get; set; }
    }
}
