﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace library_project.Migrations
{
    public partial class bookSNE : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "bookCode",
                table: "Book",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "bookCode",
                table: "Book");
        }
    }
}
