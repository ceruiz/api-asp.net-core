﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace library_project.Migrations
{
    public partial class secondVersion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "catName",
                table: "Category",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "autCountry",
                table: "Author",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "catName",
                table: "Category",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "autCountry",
                table: "Author",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
